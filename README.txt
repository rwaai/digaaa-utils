A few utilities for the digaaa project

Note:
These scripts are of more didactic than absolute value.
They hardly do anything you couldn't do from the
shell, but are rather intended as an introduction to xml handling,
file system navigation etc. for prospect python programmers.

replace_eaf
   reads a tab-separated file of oldid-newid pairs;
   looks for oldid in certain (hardcoded, see source)
   attributes of an elan annotation file (eaf) and replaces
   them with newid.

   to test, download, unpack, cd into, then
   $ cd replace_eaf
   $ diff test/sample.eaf test/sample.eaf.orig
   $ ./replace_eaf.py test/old_new_ids.tsv test/sample.eaf
   wrote new xml to test/sample.eaf with backup to test/sample.eaf.bup
   $ diff sample.eaf sample.eaf.orig
   4c4
<         <MEDIA_DESCRIPTOR MEDIA_URL="mynewid1.wav" MIME_TYPE="audio/x-wav" RELATIVE_MEDIA_URL="KJGKLAR730305_01A_S01.wav" />
---
>         <MEDIA_DESCRIPTOR MEDIA_URL="myoldid1.wav" MIME_TYPE="audio/x-wav" RELATIVE_MEDIA_URL="KJGKLAR730305_01A_S01.wav" />
1543c1543

  (there may be some additional insigificant differences in whitespace)

rename_tsv
   does bulk renaming of files, where oldname-newname are
   read from a tab-separated file. Performs some useful error checks,
   but could otherwise in principle have been replaced by shell 'rename'
   and a tiny bit of bash chops.

   to test: download, unpack, cd into, then

   $ cd rename_tsv
   $ ./rename_tsv.py renames.txt


