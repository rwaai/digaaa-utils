#!/usr/bin/python3

import os, shutil, sys 
import xml.etree.ElementTree as ET

def read_replacements(tsvfile):
    replacements = dict()
    with open(tsvfile) as f:
        for line in f:
            (old, new) = line.split()
            replacements[old] = new
    return replacements
    
def read_xml(xmlfile):
    tree = ET.parse(xmlfile)
    return tree
        
def process(xmlfile, replacements, bupfile):     
    xpath = "HEADER/MEDIA_DESCRIPTOR[@MEDIA_URL]"
    changeattrs = ["MEDIA_URL", "RELATIVE_MEDIA_URL"]
    
    xml = read_xml(xmlfile)
    #NOTE: there may be any number of media descriptors 
    #http://www.mpi.nl/tools/elan/EAF_Annotation_Format.pdf        
    mediadescs = xml.findall(xpath)
    for mdesc in mediadescs:     
        for urlattr in changeattrs:
            oldpath = mdesc.get(urlattr, "")
            (_olddir, oldfile) = os.path.split(oldpath)
            (oldbase, fileext) = os.path.splitext(oldfile)
            newfile = replacements.get(oldbase, oldbase) + fileext
            mdesc.set(urlattr, newfile)
    shutil.copy2(xmlfile, bupfile)
    xml.write(xmlfile, encoding="utf-8", xml_declaration=True)
    print("wrote new xml to {} with backup to {}".format(
    	                 xmlfile,         bupfile), file=sys.stderr)
def main():
    if len(sys.argv) <= 1:
        raise SystemExit(
            "Usage: {} replacements.tsv eaffile1.eaf [eaffile2.eaf...]".format(
                   sys.argv[0]))
    (infile, *xmlfiles) = sys.argv[1:]        # *arg-syntax is python3-only
    replacements = read_replacements(infile)
    for xmlfile in xmlfiles:
        bupfile = xmlfile.replace(".eaf", ".eaf.bup")
        process(xmlfile, replacements, bupfile)


if __name__ == '__main__':
    main()


