#!/usr/bin/python3

import argparse, logging, os, sys

logging.basicConfig(format='%(levelname)s: %(message)s')
 
def read_renames(infile):
    renames = []
    with open(infile) as f:
        for line in f:
            if not line.strip() or line.startswith("#"): 
                continue
            (oldname, newname) = line.strip().split("\t")
            renames.append((oldname, newname))
    return renames            
    
def rename(renames, dryrun=True):
    for (oldname, newname) in renames:
        if not os.path.exists(oldname):
            logging.warn("source '{}' does not exist, skipping".format(oldname))
            continue
        if os.path.exists(newname):
            logging.warn("target '{}' already exists, skipping".format(newname))
            continue
        if dryrun:
            print("DRY RUN: {} ==> {}".format(oldname, newname))            
        else:
            print("RENAMED: {} ==> {}".format(oldname, newname))            
            os.rename(oldname, newname)            
        
def get_args():
    parser = argparse.ArgumentParser(
        description='File renaming utility, with safety net')
    parser.add_argument('infile', metavar="FILE",
        help="""text file where each line in renames-file is of form 
        <oldname>TAB<newname>;
        lines starting with "#" are ignored, as are blank lines. """)
    parser.add_argument('-d', '--dry-run', default=False, action="store_true",
        help='prints what would be changed without actually doing anything')
    args = parser.parse_args()
    print(args)
    return args        
        
def main():
    args = get_args()
    renames = read_renames(args.infile)
    rename(renames, args.dry_run)
    
if __name__ == '__main__':
    main()
 

